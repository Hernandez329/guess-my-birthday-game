name = input('Hi! What is your name?  ')

if name[0].isalpha():
    print(f'Hi! {name}!' )
    istrue = True
else:
    print('Enter your name! Try Again!')
    istrue = False
    exit

playgame = False

while istrue == True :
    print(f"{name}, Want to play a guessing game?")
    answer = input('Yes or No? ').lower()

    if answer == 'no':
        print('Sucks! We are playing!')
        playgame = True
        break
    else :
        print('Great! Let me think...')
        playgame = True
        break

if playgame == True:
    while playgame == True and range(5):
        for guess in range(1, 6):
            
            import random
            month = random.randint(1, 12)

            year = random.randint(1910, 2022)

            print("Guess",guess,': ', name,', were you born in?', month,"/",year,"?")
            response = input("Yes or No? ").lower()

            if guess == 5:
                print('I have other things to do. Good bye. ')
                exit()
            if response == 'no' and guess < 5:
                print('Drat! lemme try again!')
                continue
            elif response == 'yes':
                print('I knew it!')
                exit()

else:
    exit()

